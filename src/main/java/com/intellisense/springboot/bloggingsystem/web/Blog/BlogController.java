package com.intellisense.springboot.bloggingsystem.web.Blog;

import com.intellisense.springboot.bloggingsystem.blog.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class BlogController {


    @Autowired
    private UserService userService;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private final MessageSource messageSource;

    public BlogController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @RequestMapping(value = "/blog-single", method = RequestMethod.GET)
    public String blogSingle(){

        logger.info("GET INTO BLOG SINGLE-PAGE PAGE");

        return "blog/blog-single";
    }


}
