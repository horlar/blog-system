package com.intellisense.springboot.bloggingsystem.web.portfolio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PortfolioController {

    Logger logger = LoggerFactory.getLogger(this.getClass());


    @RequestMapping(value = "/portfolio", method = RequestMethod.GET)
    public String portfolio(){

        logger.info("GET INTO PORTFOLIO PAGE");
        return "index";
    }
}
