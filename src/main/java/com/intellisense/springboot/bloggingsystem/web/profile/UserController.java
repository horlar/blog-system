package com.intellisense.springboot.bloggingsystem.web.profile;

import com.intellisense.springboot.bloggingsystem.blog.user.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/my-account", method = RequestMethod.GET)
    public String userAccount(Model model, User user){
        model.addAttribute("user", user);
        logger.info("GET INTO USER ACCOUNT PAGE");

        return "user-account/account";
    }
}
