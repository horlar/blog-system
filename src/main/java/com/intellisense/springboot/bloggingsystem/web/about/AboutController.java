package com.intellisense.springboot.bloggingsystem.web.about;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AboutController {
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String aboutPage(){
        logger.info("GOT INTO ABOUT PAGE");
        return "about/about";
    }
}
