package com.intellisense.springboot.bloggingsystem.web.register;

import com.intellisense.springboot.bloggingsystem.blog.user.dto.UserDto;
import com.intellisense.springboot.bloggingsystem.blog.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Locale;

@Controller
public class RegisterController {
    @Autowired
    private UserService userService;

    private final MessageSource messageSource;

    public RegisterController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registration(Model model){
        model.addAttribute("user", new UserDto());

        return "security/login";
    }

    @RequestMapping(value = "/register/user", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") UserDto userDto, BindingResult result, RedirectAttributes redirectAttributes, Model model, Locale locale){

        if (result.hasErrors()) {
            model.addAttribute("errorMessage", messageSource.getMessage("form.fields.required", null, locale));
            return "security/login";
        }

        try{
            //create a new user
            String user = userService.create(userDto);
            //if user data is not empty give a successful message
            redirectAttributes.addFlashAttribute("successMessage", user);
            return "redirect:/register";
        }catch (NullPointerException e){
            return e.getMessage();
        }
    }
}
