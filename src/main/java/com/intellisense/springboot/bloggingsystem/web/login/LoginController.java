package com.intellisense.springboot.bloggingsystem.web.login;

import com.intellisense.springboot.bloggingsystem.blog.user.model.User;
import com.intellisense.springboot.bloggingsystem.blog.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Locale;

@Controller
public class LoginController {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private final MessageSource messageSource;

    @Autowired
    private UserService userService;

    public LoginController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, User user){
        model.addAttribute("user", user);
        logger.info("GET INTO LOGIN PAGE");

        return "security/login";
    }

    @RequestMapping(value = "/user/login", method = RequestMethod.POST)
    public String authorLogin(@Valid User user, BindingResult result, RedirectAttributes attributes, Model model, Locale locale){

        if(result.hasErrors()){
            model.addAttribute("errorMessage",messageSource.getMessage("form.fields.required",null,locale));
            return "security/login";
        }

        if (!userService.authenticate(user.getUsername(), user.getPassword())) {
            logger.info("GOT HERE");
            model.addAttribute("errorMessage",messageSource.getMessage("user.invalid.credentials",null,locale));
            return "security/login";
        }

        //notifyService.addInfoMessage("Login successful");
        return "user-account/account";
    }
}
