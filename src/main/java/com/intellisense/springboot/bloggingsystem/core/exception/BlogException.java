package com.intellisense.springboot.bloggingsystem.core.exception;

public class BlogException extends RuntimeException {


    String message;
    Object obj;

    public BlogException() {
        super("Failed to perform the requested action");
    }

    public BlogException(Throwable cause) {
        super("Failed to perform the requested action", cause);
    }

    public BlogException(String message) {
        super(message);
    }

    public BlogException(String message, Throwable cause) {
        super(message, cause);
    }

    public BlogException(String message, Object obj) {
        this.message = message;
        this.obj = obj;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }
}
