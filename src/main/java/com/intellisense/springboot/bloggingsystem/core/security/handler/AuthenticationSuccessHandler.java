package com.intellisense.springboot.bloggingsystem.core.security.handler;

import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
}
