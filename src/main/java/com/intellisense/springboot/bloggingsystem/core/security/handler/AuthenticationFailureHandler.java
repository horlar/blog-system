package com.intellisense.springboot.bloggingsystem.core.security.handler;

import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
}
