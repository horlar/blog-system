package com.intellisense.springboot.bloggingsystem.core.setup;

import com.intellisense.springboot.bloggingsystem.blog.Role.dto.RoleDto;
import com.intellisense.springboot.bloggingsystem.blog.Role.model.Role;
import com.intellisense.springboot.bloggingsystem.blog.Role.model.RoleType;
import com.intellisense.springboot.bloggingsystem.blog.user.model.User;
import com.intellisense.springboot.bloggingsystem.blog.Role.repository.RoleRepository;
import com.intellisense.springboot.bloggingsystem.blog.user.repository.UserRepository;
import com.intellisense.springboot.bloggingsystem.blog.Role.service.RoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SetupConfiguration implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleService roleService;
    @Autowired
    private RoleRepository roleRepository;

    @Bean
    public PasswordEncoder passwordEncoder() {

        return new BCryptPasswordEncoder();
    }



    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void run(String... args) throws Exception {

        logger.info("######### SETTING UP SUPER ADMIN USER #########");

        if (shouldCreateDefaultSuperAdmin()) {
            // if default superadmin does not exist
            createDefaultSuperAdminRoleAndUser();
        }

    }

    @Transactional
    private void createDefaultSuperAdminRoleAndUser() {

        logger.info("Creating default super admin role and user");

        try {
            //check if role ADMIN is empty
            Role role = roleRepository.findFirstByNameIgnoreCase("ADMIN");

            if (role == null) {
                // if empty
                logger.info("No super admin role found, will try to create one");

                RoleDto roleDTO = new RoleDto();
                roleDTO.setName("Super Admin");
                roleDTO.setRoleType(RoleType.ADMIN);
                roleDTO.setDescription("This is the default super admin role that has all the permissions");

                role = roleService.createRole(roleDTO);
                logger.info("Created super admin role [{}]", role.getName());
            }

            // Now create a super admin user
            User superAdmin = new User();
            superAdmin.setUsername("superadmin");
            superAdmin.setFirstName("Super");
            superAdmin.setLastName("Admin");
            superAdmin.setEmail("admin@gmail.com");
            superAdmin.setRoleType(role);
            superAdmin.setPassword(passwordEncoder().encode("superadmin"));
            userRepository.save(superAdmin);


        } catch (Exception e) {
            logger.error("Failed to create super admin", e);
        }

    }

    private boolean shouldCreateDefaultSuperAdmin() {

        logger.info("Checking if super admin user should be created");

        //check if a user with username superadmin already exists
        boolean superAdminExists = userRepository.existsByUsername("superadmin");

        if (!superAdminExists) {
            // if not
            logger.info("Super admin will be created");
            return true;

        }

        // if already exists
        logger.info("Super admin will not be created");
        return false;
    }
}
