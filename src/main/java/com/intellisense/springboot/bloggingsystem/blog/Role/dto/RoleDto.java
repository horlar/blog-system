package com.intellisense.springboot.bloggingsystem.blog.Role.dto;

import com.intellisense.springboot.bloggingsystem.blog.Role.model.RoleType;

public class RoleDto {

    private String name;

    private RoleType roleType;

    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "RoleDto{" +
                "name='" + name + '\'' +
                ", roleType=" + roleType +
                ", description='" + description + '\'' +
                '}';
    }
}
