package com.intellisense.springboot.bloggingsystem.blog.Role.service.serviceimplem;

import com.intellisense.springboot.bloggingsystem.blog.Role.dto.RoleDto;
import com.intellisense.springboot.bloggingsystem.core.exception.BlogException;
import com.intellisense.springboot.bloggingsystem.blog.Role.model.Role;
import com.intellisense.springboot.bloggingsystem.blog.Role.repository.RoleRepository;
import com.intellisense.springboot.bloggingsystem.blog.Role.service.RoleService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

@Service
public class RoleServiceImplementation implements RoleService {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private MessageSource messageSource;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private Locale locale = LocaleContextHolder.getLocale();


    private ModelMapper modelMapper = new ModelMapper();

    @Override
    @Transactional
    public Role createRole(RoleDto roleDTO) {
        Role role = roleRepository.findFirstByNameIgnoreCase(roleDTO.getName());

        if (role != null) {
            throw new BlogException(messageSource.getMessage("role.exist", null, locale));
        }

        try {
            role = convertDTOToEntity(roleDTO);
            roleRepository.save(role);
            logger.info("Added role {}", role.toString());
            return role;
        } catch (BlogException e) {
            throw new BlogException(messageSource.getMessage("role.add.failure", null, locale), e);
        }
    }

    private Role convertDTOToEntity(RoleDto roleDTO) {
        Role role = modelMapper.map(roleDTO, Role.class);
        role.setRoleType(roleDTO.getRoleType());
        return role;
    }
}
