package com.intellisense.springboot.bloggingsystem.blog.user.service;

import com.intellisense.springboot.bloggingsystem.blog.user.dto.UserDto;
import com.intellisense.springboot.bloggingsystem.blog.user.model.User;

import java.util.List;
import java.util.Optional;


public interface UserService {


    List<User> findAll();

    Optional<User> findById(Long id);

    String create(UserDto userDto);

    String findByUsername(String username);

    User edit(User user);

    void deleteById(Long id);

    boolean authenticate(String username, String Password);
}
