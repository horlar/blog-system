package com.intellisense.springboot.bloggingsystem.blog.user.repository;

import com.intellisense.springboot.bloggingsystem.blog.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository  extends JpaRepository<User, Long> {

    User findByUsername(String username);

    boolean existsByUsername(String username);
}
