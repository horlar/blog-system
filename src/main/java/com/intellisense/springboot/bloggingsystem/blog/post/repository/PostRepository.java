package com.intellisense.springboot.bloggingsystem.blog.post.repository;

import com.intellisense.springboot.bloggingsystem.blog.post.model.Post;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    @Query("SELECT p from Post p LEFT join FETCH p.author order by p.date desc ")
    List<Post> findLatestPost(Pageable pageable);

}
