package com.intellisense.springboot.bloggingsystem.blog.post.dto;

import com.intellisense.springboot.bloggingsystem.blog.user.model.User;

public class PostDto {

    private String title;
    private String body;
    private User author;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "PostDto{" +
                "title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", author=" + author +
                '}';
    }
}
