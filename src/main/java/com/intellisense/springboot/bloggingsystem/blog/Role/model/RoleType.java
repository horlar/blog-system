package com.intellisense.springboot.bloggingsystem.blog.Role.model;

public enum  RoleType {

    USER, ADMIN;
}
