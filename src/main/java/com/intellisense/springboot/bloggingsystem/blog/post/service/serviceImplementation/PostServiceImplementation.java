package com.intellisense.springboot.bloggingsystem.blog.post.service.serviceImplementation;

import com.intellisense.springboot.bloggingsystem.blog.post.model.Post;
import com.intellisense.springboot.bloggingsystem.blog.post.repository.PostRepository;
import com.intellisense.springboot.bloggingsystem.blog.post.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PostServiceImplementation implements PostService {

    @Autowired
    private PostRepository postrepository;

    @Override
    public List<Post> findAll() {
        return this.postrepository.findAll();
    }

    @Override
    public List<Post> findLatest() {
        return this.postrepository.findLatestPost(new PageRequest(0, 1));
    }

    @Override
    public Optional<Post> findById(Long id) {
        return this.postrepository.findById(id);
    }

    @Override
    public Post create(Post post) {
        return this.postrepository.save(post);
    }

    @Override
    public Post edit(Post post) {
        return this.postrepository.save(post);
    }

    @Override
    public void deleteById(Long id) {
        this.postrepository.deleteById(id);
    }

}
