package com.intellisense.springboot.bloggingsystem.blog.post.service;

import com.intellisense.springboot.bloggingsystem.blog.post.model.Post;

import java.util.List;
import java.util.Optional;

public interface PostService {
    List<Post> findAll();

    List<Post> findLatest();

    Optional<Post> findById(Long id);

    Post create(Post post);

    Post edit(Post post);

    void deleteById(Long id);
}
