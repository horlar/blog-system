package com.intellisense.springboot.bloggingsystem.blog.Role.service;

import com.intellisense.springboot.bloggingsystem.blog.Role.dto.RoleDto;
import com.intellisense.springboot.bloggingsystem.blog.Role.model.Role;

public interface RoleService {

    Role createRole(RoleDto roleDTO);

}
