package com.intellisense.springboot.bloggingsystem.blog.user.service.serviceImplementation;

import com.intellisense.springboot.bloggingsystem.blog.user.dto.UserDto;
import com.intellisense.springboot.bloggingsystem.blog.user.model.User;
import com.intellisense.springboot.bloggingsystem.blog.user.repository.UserRepository;
import com.intellisense.springboot.bloggingsystem.blog.user.service.UserService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserServiceImplementation implements UserService {

    @Autowired
    private UserRepository userRepo;
    @Autowired
    private MessageSource messageSource;

    private ModelMapper modelMapper = new ModelMapper();

    private final Locale locale = LocaleContextHolder.getLocale();
    Logger logger = LoggerFactory.getLogger(this.getClass());



    @Override
    public List<User> findAll() {
        return this.userRepo.findAll();
    }

    @Override
    public Optional<User> findById(Long id) {
        return this.userRepo.findById(id);
    }

    @Override
    public String create(UserDto userDto) {

        User user = modelMapper.map(userDto, User.class);
//        user.setRoleType(RoleType.USER);

        try {
            userRepo.save(user);

            return messageSource.getMessage("user.create.success", null, locale);

        } catch (Exception ex) {
            return messageSource.getMessage("user.create.error", null, locale);
        }
    }

    @Override
    public String findByUsername(String username) {

        User user = userRepo.findByUsername(username);


        logger.debug("Validating a login user {}", user.getEmail());

        if (Objects.nonNull(user)) {
            logger.info("username exist ,welcome to the intellisense blog post");
        }
        return null;
    }

    @Override
    public User edit(User user) {
        return this.userRepo.save(user);
    }

    @Override
    public void deleteById(Long id) {
        this.userRepo.deleteById(id);
    }

    @Override
    public boolean authenticate(String username, String password) {

        //autenticate a user
        logger.debug("Authenticating user [{}] ", username);
        return Objects.equals(username,password);
    }
}
