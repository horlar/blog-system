package com.intellisense.springboot.bloggingsystem.blog.Role.repository;

import com.intellisense.springboot.bloggingsystem.blog.Role.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findFirstByNameIgnoreCase(String s);

}
